from rest_framework.decorators import api_view
from rest_framework.response import Response
from division.task import div

# Create your views here.

@api_view(['GET', 'POST'])
def division(request):
    if request.method == 'POST':
        div(request.data["num_1"], request.data["num_2"])
        return Response({"message": "div published task"})
    return Response({ "num_1": 1, "num_2": 2 })