
from django.urls import path
from division.views import division


urlpatterns = [
    path('division', division, name='division'),
]