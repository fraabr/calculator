from calculator.celery import app
from division.models import Division

@app.task(name='calculator.div')
def div(payload):
    print("running task calculator.div with payload" + str(payload))
    Division.objects.create(number_1 = payload["num_1"], number_2 = payload["num_2"])