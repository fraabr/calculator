from rest_framework.decorators import api_view
from rest_framework.response import Response
from substraction.task import sub

# Create your views here.

@api_view(['GET', 'POST'])
def substraction(request):
    if request.method == 'POST':
        sub(request.data["num_1"], request.data["num_2"])
        return Response({"message": "sub published task"})
    return Response({ "num_1": 1, "num_2": 2 })