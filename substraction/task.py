from calculator.celery import app
from substraction.models import Substraction

@app.task(name='calculator.sub')
def sub(payload):
    print("running task calculator.sub with payload" + str(payload))
    Substraction.objects.create(number_1 = payload["num_1"], number_2 = payload["num_2"])
