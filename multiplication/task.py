from calculator.celery import app
from multiplication.models import Multiplication

@app.task(name='calculator.mul')
def mul(payload):
    print("running task calculator.mul with payload" + str(payload))
    Multiplication.objects.create(number_1 = payload["num_1"], number_2 = payload["num_2"])