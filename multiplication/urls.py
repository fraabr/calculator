
from django.urls import path
from multiplication.views import multiplication


urlpatterns = [
    path('multiplication', multiplication, name='multiplication'),
]