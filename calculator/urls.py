"""calculator URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from addition.urls import urlpatterns as addition_urls
from division.urls import urlpatterns as division_urls
from multiplication.urls import urlpatterns as multiplication_urls
from substraction.urls import urlpatterns as substraction_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(addition_urls)),
    path('', include(division_urls)),
    path('', include(multiplication_urls)),
    path('', include(substraction_urls)),
]
