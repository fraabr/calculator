from django.db import models

# Create your models here.
class Addition(models.Model):
    number_1 = models.IntegerField()
    number_2 = models.IntegerField()
    total = models.IntegerField()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.total = int(self.number_1) + int(self.number_2)
        super(Addition, self).save(*args, **kwargs)
