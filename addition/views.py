from rest_framework.decorators import api_view
from rest_framework.response import Response
from addition.task import add

# Create your views here.

@api_view(['GET', 'POST'])
def addition(request):
    if request.method == 'POST':
        add(request.data["num_1"], request.data["num_2"])
        return Response({"message": "sum published task"})
    return Response({ "num_1": 1, "num_2": 2 })