from calculator.celery import app
from addition.models import Addition

@app.task(name='calculator.add')
def add(payload):
    print("running task calculator.add with payload " + str(payload))
    Addition.objects.create(number_1 = payload["num_1"], number_2 = payload["num_2"])